// console.log('hello')

// Objects
	/*
	    - An object is a data type that is used to represent real world objects
	    - It is a collection of related data and/or functionalities
	    - In JavaScript, most core JavaScript features like strings and arrays are objects (Strings are a collection of characters and arrays are a collection of data)
	    - Information stored in objects are represented in a "key:value" pair
	    - A "key" is also mostly referred to as a "property" of an object
	    - Different data types may be stored in an object's property creating complex data structures
	*/


// Create object using object initializers/ literal notation
/*
	- This creates/declares an object and also initializes/assigns it's properties upon creation
	- A cellphone is an example of a real world object
	- It has it's own properties such as name, color, weight, unit model and a lot of other things
	
	Syntax:
		let objectName = {
			keyA: valueA,
			keyB: valueB
		}
*/
let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999	
};
console.log('Object created through initializer:');
console.log(cellphone);
console.log(typeof cellphone);

// Create object using constructor function
/*
	- Creates a reusable function to create several objects that have the same data structure
	- This is useful for creating multiple instances/copies of an object
	- An instance is a concrete occurence of any object which emphasizes on the distinct/unique identity of it
	
	Syntax:
		function objectName(keyA, keyB){
			this.keyA = keyA;
			this.keyB = keyB;
		}

*/

// This is an object
// The "this" keyword allows to assign a new object's properties by associating them with values received from a constructor function's parameters
function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
};

/*
    - The "new" operator creates an instance of an object
    - Objects and instances are often interchanged because object literals (let object = {}) and instances (let object = new object) are distinct/unique objects
*/

let laptop = new Laptop('Lenovo', 2008);
console.log('Object through constructor function:')
console.log(Laptop);

let myLaptop = new Laptop('Macbook Air', 2020);
console.log('Object through constructor function again:');
console.log(myLaptop);

/*
    - The example above invokes/calls the "Laptop" function instead of creating a new object instance
    - Returns "undefined" without the "new" operator because the "Laptop" function does not have a return statement
*/
let oldLaptop = Laptop('Portal R2E CCMC', 1980);
console.log('Object through constructor function without new keyword:');
console.log(oldLaptop);
// undefined

Laptop('Portal R2E CCMC', 1980);
// no result from plain invocation

// Creating empty objects
let computer = {};
let myComputer = new Object();

// Accessing object properties

// using dot notation
console.log('Result from dot notation: ' + myLaptop.name);
// Syntax: objectName.property

// using square bracket notation
console.log('Result from bracket notation: ' + myLaptop['name']);
// Syntax: objectName['property']

// using template literals
console.log(`Result from dot notation ${laptop.name}`);
console.log(`Result from dot notation with non-existing property ${laptop.specs}`);
// result: undefined

// Accessing array objects
/*
    - Accessing array elements can be also be done using square brackets
    - Accessing object properties using the square bracket notation and array indexes can cause confusion
    - By using the dot notation, this easily helps us differentiate accessing elements from arrays and properties from objects
    - Object properties have names that makes it easier to associate pieces of information
*/

let array = [laptop, myLaptop];

// bracket notation
// May be confused for accessing array indexes
console.log(`Result from bracket notation: ${array[0]['name']}`);

// dot notation
// This tells us that array[1] is an object by using the dot notation
console.log(`Result from dot notation: ${array[1].manufactureDate}`);

/*
	Mini-Activity: 9 mins
		>> Create an object with the following key:value pair

			firstName: string
			lastName: string
			password: string
			email: string
			age: number
		
		>> Access each property's values
			>> 5 console.log showing all values of the object
	
		>> Send your output in hangouts
*/
// S O L U T I O N

let person = {

	firstName: 'Levi',
	lastName: 'Ackerman',
	password: 'notInAWheelchair',
	email: 'levi@mail.com',
	age: 30
};

console.log(`first name: ${person.firstName}`);
console.log(`last name: ${person.lastName}`);
console.log(`password: ${person.password}`);
console.log(`email: ${person.email}`);
console.log(`age: ${person.age}`);

// Initialize / add object properties using dot notation
/*
    - Like any other variable in JavaScript, objects may have their properties initialized/added after the object was created/declared
    - This is useful for times when an object's properties are undetermined at the time of creating them
*/

let car = {};

car.name = 'Honda Civic';
console.log('Result from adding property through dot notation: ');
console.log(car);

// using bracket notation
car['manufacture date'] = 2019
console.log(`Result from adding property through bracket notation:`);
console.log(car);

// deleting object properties
 delete car['manufacture date'];
 console.log('Result from deleting:');
 console.log(car);

// re-assigning object properties
car.name = 'Dodge Charger R/T';
console.log('Result from reassigment:');
console.log(car);

// Object Methods

let person1 = {
	name: 'John',
	talk: function(){
		console.log(`Hello my name is ${this.name}`);
	}
};
console.log(person1);
console.log('Result from object method:')
person1.talk();

// adding methods to our objects
person1.walk = function(){
	console.log(`${this.name} walked 25 steps forward`);
}
person1.walk();

let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address: {
		city: 'Austin',
		country: 'Texas'
	},
	emails: ['joe@mail.com', 'joesmith@email.xyz'],
	introduce: function(){
		console.log(`Hello my name is ${this.firstName} ${this.lastName}. I live at ${this.address.city}, ${this.address.country} and my personal email is ${this.emails[0]}`)
	}
};
friend.introduce();
// console.log(`1. Hello my name is ${friend.firstName} ${friend.lastName}. I live at ${friend.address.city}, ${friend.address.country} and my personal email is ${friend.emails[0]}`)

// Real world application of objects

// Object constructor

function Pokemon(name, level){

	// Properties
	this.name = name;
	this.lvl = level;
	this.health = 2 * level;
	this.atk = level;

	// Methods
	this.tackle = function(target){
		console.log(`${this.name} tackled ${target.name}`)
		console.log(`targetPokemon's health is now reduced to _targetPokemonHealth_`);
	};

	this.faint = function(){
		console.log(`${this.name} fainted`)
	};
};

// Creates new instances of the "Pokemon" object each with their unique properties

let bulbasaur = new Pokemon("bulbasaur", 16);
let rattata = new Pokemon("rattata", 8);

console.log(bulbasaur);
console.log(rattata);

// Providing the "rattata" object as an argument to the "bulbasaur" tackle method will create interaction between the two objects
bulbasaur.tackle(rattata);
rattata.tackle(bulbasaur);
bulbasaur.faint();
